//
//  ViewController.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 11/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit
import WeatherLib

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let weatherLib = WeatherLib(key: "4ca220bf7041f7c0c7293e631e513c5b")
        weatherLib.getWeather(latitude: 33.441792, longitude: -94.037689) { forecast in
            print(forecast)
        }
    }


}

