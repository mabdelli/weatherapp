//
//  UITableViewCell.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 13/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit

extension UITableViewCell: ReusableView {}
