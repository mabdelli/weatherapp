//
//  UICollectionViewCell.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 16/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit

extension UICollectionViewCell: ReusableView {}
