//
//  UICollectionView.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 16/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit

extension UICollectionView {
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T? {
        return dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T
    }
}
