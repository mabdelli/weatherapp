//
//  CommonNavigationDelegate.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 13/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit

protocol ViewModelNavigationDelegate {
    func presentAlert(on viewController: UIViewController?, title: String?, message: String?, action: (() -> Void)?)
}

extension ViewModelNavigationDelegate {
    func presentAlert(on viewController: UIViewController?, title: String?, message: String?, action: (() -> Void)?) {
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: localize("OK"), style: .default, handler: { _ in
            action?()
        })
        alertController.addAction(defaultAction)
        viewController?.present(alertController, animated: true)
    }
}
