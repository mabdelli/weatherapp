//
//  ReusableView.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 13/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import Foundation

protocol ReusableView {
    static var reuseIdentifier: String { get }
}

extension ReusableView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
