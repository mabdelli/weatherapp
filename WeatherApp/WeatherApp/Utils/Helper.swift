//
//  Helper.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 11/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import Foundation

func localize(_ value: String) -> String {
    return NSLocalizedString(value, comment: "")
}

func log(_ item: Any) {
    if (Int(Environment.enableLog.value()) == 1) {
        print(item)
    }
}

func flag(country: String) -> String {
    let base: UInt32 = 127397
    var s = ""
    for v in country.unicodeScalars {
        s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
    }
    return s
}
