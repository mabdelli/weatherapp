//
//  AppDelegate.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 11/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var homeCoordinator: HomeCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow()
        homeCoordinator = HomeCoordinator()
        window?.rootViewController = homeCoordinator?.navigationController
        window?.makeKeyAndVisible()
        
        return true
    }

}

