//
//  DetailCityViewModel.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 15/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import Foundation
import WeatherLib

protocol DetailCityViewModelDelegate: AnyObject {
    func didLoadForecast()
}

protocol DetailCityViewModelNavigationDelegate: ViewModelNavigationDelegate, AnyObject {
    func presentAlert(title: String?, message: String?, action: (()->())?)
}

class DetailCityViewModel {
    
    private var name: String
    private var latitude: Float
    private var longitude: Float
    private var weather: Weather
    weak private var delegate: DetailCityViewModelDelegate?
    weak private var navigationDelegate: DetailCityViewModelNavigationDelegate?
    private var forecast: Forecast?

    init(name: String,
         latitude: Float,
         longitude: Float,
         weather: Weather = Weather(key: Environment.openWeatherApiKey.value()),
         delegate: DetailCityViewModelDelegate?,
         navigationDelegate: DetailCityViewModelNavigationDelegate?) {
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.weather = weather
        self.delegate = delegate
        self.navigationDelegate = navigationDelegate
    }
    
    func loadForecast() {
        weather.getForecast(latitude: latitude, longitude: longitude) { [weak self] (result) in
            
            switch result {
            case .success(let forecast):
                self?.forecast = forecast
            case .failure(let error):
                let title = localize("Warning")
                var message: String?
                switch error {
                case .noNetwork:
                    message = localize("NoNetwork")
                default:
                    message = localize("DefaultNetworkError")
                }
                self?.navigationDelegate?.presentAlert(title: title, message: message, action: nil)
            }
            self?.delegate?.didLoadForecast()
        }
    }
    
    func getTitle() -> String {
        return name
    }
    
    func nbItems() -> Int {
        forecast?.icons.count ?? 0
    }
    
    func item(at index: Int) -> String? {
        guard let nbIcons = forecast?.icons.count, index < nbIcons else {
            return nil
        }
        
        return forecast?.icons[index]
    }
    
    func getTemperature() -> String {
        if let temperature = forecast?.temperature {
            return Temperature.celsius.format(value: temperature)
        }
        
        return "--"
    }
    
    func getPressure() -> String {
        if let pressure = forecast?.pressure {
            return "\(pressure) hPA"
        }
        
        return "--"
    }
    
    func getHumidity() -> String {
        if let humidity = forecast?.humidity {
            return "\(humidity) %"
        }
        
        return "--"
    }
    
    func getWindSpeed() -> String {
        if let windSpeed = forecast?.windSpeed {
            return "\(windSpeed) m/s"
        }
        
        return "--"
    }
    
    func getWindTransformValue() -> Float? {
        if let windDeg = forecast?.windDeg {
            return Float(windDeg) * Float.pi/180
        }
        
        return nil
    }
}
