//
//  DetailCityCoordinator.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 15/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit

class DetailCityCoordinator {

    weak var viewController: DetailCityViewController?
    
    init(name: String, latitude: Float, longitude: Float) {
        configureDependencies(name: name, latitude: latitude, longitude: longitude)
    }
    
    func configureDependencies(name: String, latitude: Float, longitude: Float) {
        let storyboard = UIStoryboard(name: "DetailCity", bundle: Bundle.main)
        viewController = storyboard.instantiateInitialViewController() as? DetailCityViewController
        
        let detailCityViewModel = DetailCityViewModel(name: name,
                                                      latitude: latitude,
                                                      longitude: longitude,
                                                      delegate: viewController,
                                                      navigationDelegate: self)
        viewController?.viewModel = detailCityViewModel
    }
}

extension DetailCityCoordinator: DetailCityViewModelNavigationDelegate {
    func presentAlert(title: String?, message: String?, action: (() -> ())?) {
        presentAlert(on: viewController, title: title, message: message, action: action)
    }
}
