//
//  ImageCollectionViewCell.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 16/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit
import WeatherLib

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: ImageView!
    
    func fill(with url: String) {
        imageView.download(from: url)
    }
}
