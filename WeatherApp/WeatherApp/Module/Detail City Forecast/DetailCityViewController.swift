//
//  DetailCityViewController.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 15/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit

class DetailCityViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var temperatureTitleLabel: UILabel!
    @IBOutlet weak var pressureTitleLabel: UILabel!
    @IBOutlet weak var humidityTitleLabel: UILabel!
    @IBOutlet weak var windSpeedTitleLabel: UILabel!
    @IBOutlet weak var windDegTitleLabel: UILabel!
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windDegImage: UIImageView!
    
    var viewModel: DetailCityViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = viewModel?.getTitle()
        viewModel?.loadForecast()
    }
    
    func refreshView() {
        collectionView.reloadData()
        temperatureTitleLabel.text = localize("DetailCityViewController.feels.title")
        pressureTitleLabel.text = localize("DetailCityViewController.pressure.title")
        humidityTitleLabel.text = localize("DetailCityViewController.humidity.title")
        windSpeedTitleLabel.text = localize("DetailCityViewController.wind.speed.title")
        windDegTitleLabel.text = localize("DetailCityViewController.wind.degree.title")
        
        temperatureLabel.text = viewModel?.getTemperature()
        pressureLabel.text = viewModel?.getPressure()
        humidityLabel.text = viewModel?.getHumidity()
        windSpeedLabel.text = viewModel?.getWindSpeed()
        if let value = viewModel?.getWindTransformValue() {
            windDegImage.transform = CGAffineTransform(rotationAngle: CGFloat(value))
        }
    }
}

extension DetailCityViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel?.nbItems() ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell: ImageCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath) {
            if let item = viewModel?.item(at: indexPath.row) {
                cell.fill(with: item)
            }
            return cell
        }
        return UICollectionViewCell()
    }
}

extension DetailCityViewController: DetailCityViewModelDelegate {
    func didLoadForecast() {
        refreshView()
    }
}
