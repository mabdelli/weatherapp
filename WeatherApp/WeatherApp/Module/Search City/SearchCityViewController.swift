//
//  SearchCityViewController.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 13/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit

class SearchCityViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    var searchController = UISearchController()

    var viewModel: SearchCityViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = localize("SearchCityViewController.title")
        configureSearchView()
    }
    
    private func configureSearchView() {
        searchController.searchBar.placeholder = localize("SearchCityViewController.searchBar.placeholder")
        searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
    }
    
    @IBAction func cancelSearchAction(_ sender: Any) {
        viewModel?.cancelSearch()
    }
    
}

extension SearchCityViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel?.nbItems() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(for: indexPath) {
            cell.textLabel?.text = viewModel?.cityValue(at: indexPath.row)
            cell.detailTextLabel?.text = viewModel?.coordinateValue(at: indexPath.row)
            return cell
        }
        
        return UITableViewCell()
    }
}

extension SearchCityViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.saveCity(at: indexPath.row)
    }
}

extension SearchCityViewController: UISearchBarDelegate {
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if let text = searchBar.text, !text.isEmpty {
            searchController.searchBar.resignFirstResponder()
            indicatorView.startAnimating()
            viewModel?.load(city: text)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel?.stopLoading()
        indicatorView.stopAnimating()
    }
}

extension SearchCityViewController: SearchCityViewModelDelegate {
    func didLoadCities() {
        indicatorView.stopAnimating()
        searchController.isActive = false
        tableView.reloadData()
    }
}
