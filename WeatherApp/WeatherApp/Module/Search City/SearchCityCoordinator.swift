//
//  SearchCityCoordinator.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 13/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit

class SearchCityCoordinator {
    
    weak var navigationController: UINavigationController?
    var onDismiss: ()->()

    init(onDismiss: @escaping ()->()) {
        self.onDismiss = onDismiss
        configureDependencies()
    }
    
    private func configureDependencies() {
        let storyboard = UIStoryboard(name: "SearchCity", bundle: Bundle.main)
        navigationController = storyboard.instantiateInitialViewController() as? UINavigationController

        if let viewController = navigationController?.viewControllers.first as? SearchCityViewController {
            let searchCityViewModel = SearchCityViewModel(delegate: viewController,
                                                          navigationDelegate: self)
            viewController.viewModel = searchCityViewModel
        }
    }
}

extension SearchCityCoordinator: SearchCityViewModelNavigationDelegate {
    func presentAlert(title: String?, message: String?, action: (() -> ())?) {
        presentAlert(on: navigationController, title: title, message: message, action: action)
    }
    
    func dismiss() {
        onDismiss()
        navigationController?.dismiss(animated: true)
    }
}
