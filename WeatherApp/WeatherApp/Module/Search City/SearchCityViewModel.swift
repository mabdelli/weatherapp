//
//  SearchCityViewModel.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 13/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import Foundation
import WeatherLib

protocol SearchCityViewModelDelegate: AnyObject {
    func didLoadCities()
}

protocol SearchCityViewModelNavigationDelegate: ViewModelNavigationDelegate, AnyObject {
    func presentAlert(title: String?, message: String?, action: (()->())?)
    func dismiss()
}

class SearchCityViewModel {
    
    private var weather: Weather
    weak private var delegate: SearchCityViewModelDelegate?
    weak private var navigationDelegate: SearchCityViewModelNavigationDelegate?
    private var cities: [City]?
    private var cancellable: Cancellable?
    
    init(weather: Weather = Weather(key: Environment.openWeatherApiKey.value()),
         delegate: SearchCityViewModelDelegate,
         navigationDelegate: SearchCityViewModelNavigationDelegate) {
        self.weather = weather
        self.delegate = delegate
        self.navigationDelegate = navigationDelegate
    }
    
    func load(city: String) {
        cancellable = weather.getCity(value: city) { [weak self] result in
            switch result {
            case .success(let cities):
                if let cities = cities {
                    if cities.count == 0 {
                        self?.navigationDelegate?.presentAlert(title: localize("Information"), message: localize("SearchCityViewModel.noCity.message"), action: nil)
                    }
                    self?.cities = cities
                }
            case .failure(let error):
                let title = localize("Warning")
                var message: String?
                switch error {
                case .noNetwork:
                    message = localize("NoNetwork")
                default:
                    message = localize("DefaultNetworkError")
                }
                self?.navigationDelegate?.presentAlert(title: title, message: message, action: nil)
            }
            self?.delegate?.didLoadCities()
        }
    }
    
    func stopLoading() {
        cancellable?.cancelRequest()
    }
    
    func cancelSearch() {
        stopLoading()
        navigationDelegate?.dismiss()
    }
    
    func saveCity(at index: Int) {
        guard let cities = cities, index < cities.count else {
            return
        }
        
        weather.saveFavoriteCity(cities[index])
        navigationDelegate?.dismiss()
    }
    
    func nbItems() -> Int {
        cities?.count ?? 0
    }
    
    func cityValue(at index: Int) -> String {
        guard let cities = cities, index < cities.count else {
            return ""
        }
        
        return "\(cities[index].name ?? ""), \(cities[index].country ?? "") \(flag(country: cities[index].country ?? ""))"
    }
    
    func coordinateValue(at index: Int) -> String {
        guard let cities = cities, index < cities.count else {
            return ""
        }
        
        return String(format: localize("Coordinate.text"), cities[index].coordinate?.latitude ?? 0, cities[index].coordinate?.longitude ?? 0)
    }
}
