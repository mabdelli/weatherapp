//
//  ViewController.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 11/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: HomeViewModel?
    let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = localize("HomeViewController.title")
        refreshControl.addTarget(self, action: #selector(refreshView), for: .valueChanged)
        tableView.refreshControl = refreshControl
        refreshView()
    }
    
    @objc
    func refreshView() {
        viewModel?.loadSavedCities(refresh: true)
    }

    @IBAction func addCitiesAction(_ sender: Any) {
        viewModel?.addCity { [weak self] in
            self?.viewModel?.loadSavedCities()
        }
    }
    
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel?.nbItems() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: HomeTableViewCell = tableView.dequeueReusableCell(for: indexPath),
            let viewModel = viewModel,
            let city = viewModel.cityValue(at: indexPath.row) {
            cell.fill(with: city)
            return cell
        }
        
        return UITableViewCell()
    }
    
}

extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.presentCity(at: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: localize("Delete")) { (action, view, handler) in
            self.viewModel?.deleteCity(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            handler(true)
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
}

extension HomeViewController: HomeViewModelDelegate {
    func didLoadFavoritesCities() {
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
}
