//
//  HomeTableViewCell.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 14/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit
import WeatherLib

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var coordinateLabel: UILabel!
    @IBOutlet weak var currentTempLabel: UILabel!
    
    func fill(with city: City) {
        nameLabel.text = "\(city.name ?? ""), \(city.country ?? "") \(flag(country: city.country ?? ""))"
        coordinateLabel.text = String(format: localize("Coordinate.text"), city.coordinate?.latitude ?? 0, city.coordinate?.longitude ?? 0)
        currentTempLabel.text = Temperature.celsius.format(value: city.temperature)
    }
}
