//
//  HomeViewModel.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 11/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import Foundation
import WeatherLib

protocol HomeViewModelDelegate: AnyObject {
    func didLoadFavoritesCities()
}

protocol HomeViewModelNavigationDelegate: ViewModelNavigationDelegate, AnyObject {
    func presentAlert(title: String?, message: String?, action: (() -> Void)?)
    func presentDetailCity(with name: String, latitude: Float, longitude: Float)
    func presentSearchCity(onDismiss: @escaping ()->())
}

class HomeViewModel {
    
    private var weather: Weather
    weak private var delegate: HomeViewModelDelegate?
    weak private var navigationDelegate: HomeViewModelNavigationDelegate?
    private var favoritesCities: [City]
    
    init(weather: Weather = Weather(key: Environment.openWeatherApiKey.value()),
         delegate: HomeViewModelDelegate,
         navigationDelegate: HomeViewModelNavigationDelegate) {
        self.weather = weather
        self.delegate = delegate
        self.navigationDelegate = navigationDelegate
        favoritesCities = []
    }
    
    func loadSavedCities(refresh: Bool = false) {
        weather.loadFavoritesCities(refresh: refresh) { [weak self] (cities, error) in
            if let error = error {
                let title = localize("Warning")
                var message: String?
                switch error {
                case .noNetwork:
                    message = localize("NoNetwork")
                default:
                    message = localize("DefaultNetworkError")
                }
                self?.navigationDelegate?.presentAlert(title: title, message: message, action: nil)
            }
            self?.favoritesCities = cities
            self?.delegate?.didLoadFavoritesCities()
        }
    }
    
    func deleteCity(at index: Int) {
        guard index < favoritesCities.count else {
            return
        }
        
        weather.deleteFavoriteCity(favoritesCities.remove(at: index))
    }
    
    func nbItems() -> Int {
        favoritesCities.count
    }
    
    func addCity(onFinish: @escaping ()->()) {
        navigationDelegate?.presentSearchCity(onDismiss: onFinish)
    }
    
    func cityValue(at index: Int) -> City? {
        guard index < favoritesCities.count else {
            return nil
        }
        
        return favoritesCities[index]
    }
    
    func presentCity(at index: Int) {
        guard index < favoritesCities.count, let coordinate = favoritesCities[index].coordinate else {
            return
        }
        let title = "\(favoritesCities[index].name ?? "") \(flag(country: favoritesCities[index].country ?? ""))"
        navigationDelegate?.presentDetailCity(with: title, latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
}
