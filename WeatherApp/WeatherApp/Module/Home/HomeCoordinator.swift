//
//  HomeCoordinator.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 11/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import UIKit

class HomeCoordinator {
    
    weak var navigationController: UINavigationController?
    var searchCityCoordinator: SearchCityCoordinator?
    var detailCityCoordinator: DetailCityCoordinator?

    init() {
        configureDependencies()
    }

    private func configureDependencies() {
        let storyboard = UIStoryboard(name: "Home", bundle: Bundle.main)
        navigationController = storyboard.instantiateInitialViewController() as? UINavigationController

        if let homeViewController = navigationController?.viewControllers.first as? HomeViewController {
            let homeViewModel = HomeViewModel(delegate: homeViewController,
                                              navigationDelegate: self)
            homeViewController.viewModel = homeViewModel
        }
    }
}

extension HomeCoordinator: HomeViewModelNavigationDelegate {

    func presentAlert(title: String?, message: String?, action: (() -> Void)?) {
        presentAlert(on: self.navigationController, title: title, message: message, action: action)
    }
    
    func presentSearchCity(onDismiss: @escaping ()->()) {
        searchCityCoordinator = SearchCityCoordinator(onDismiss: onDismiss)
        if let viewController = searchCityCoordinator?.navigationController {
            navigationController?.present(viewController, animated: true)
        }
    }
    
    func presentDetailCity(with name: String, latitude: Float, longitude: Float) {
        detailCityCoordinator = DetailCityCoordinator(name: name, latitude: latitude, longitude: longitude)
        if let viewController = detailCityCoordinator?.viewController {
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
