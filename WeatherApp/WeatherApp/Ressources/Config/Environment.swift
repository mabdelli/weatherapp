//
//  Environment.swift
//  WeatherApp
//
//  Created by Mohamed Abdelli on 11/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import Foundation

enum Environment: String {
    //Info.plist key
    case enableLog = "Enable_Log"
    case openWeatherApiKey = "Open_Weather_Api_Key"
    
    private var infoDict: [String: Any]  {
        get {
            if let dict = Bundle.main.infoDictionary {
                return dict
            } else {
                log("Plist file not found")
                return [:]
            }
        }
    }
    
    func value () -> String {
        guard let value = infoDict[self.rawValue] as? String else {
            return ""
        }
        return value
    }
    
}
