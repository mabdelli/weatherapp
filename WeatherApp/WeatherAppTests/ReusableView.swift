//
//  ReusableView.swift
//  WeatherAppTests
//
//  Created by Mohamed Abdelli on 16/08/2020.
//  Copyright © 2020 Mohamed Abdelli. All rights reserved.
//

import XCTest
@testable import WeatherApp

class ReusableView: XCTestCase {

 func testReusableIdentifier() {
     XCTAssertEqual(UICollectionViewCell.reuseIdentifier, "UICollectionViewCell")
     XCTAssertEqual(HomeTableViewCell.reuseIdentifier, "HomeTableViewCell")
     XCTAssertEqual(UITableViewCell.reuseIdentifier, "UITableViewCell")
     XCTAssertEqual(ImageCollectionViewCell.reuseIdentifier, "ImageCollectionViewCell")
 }

}
